package com.mehdi.kafka;

import com.google.gson.JsonParser;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.KStream;

import java.util.Properties;

public class KafkaStream {
    public static void main(String[] args) {
        //create properties
        var properties = new Properties();
        properties.setProperty(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "0.0.0.0:9092");
        properties.setProperty(StreamsConfig.APPLICATION_ID_CONFIG, "kafka-streams-demo");
        properties.setProperty(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.StringSerde.class
                .getName());
        properties.setProperty(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.StringSerde.class.getName());

        //create a topology
        var streamBuilder = new StreamsBuilder();

        //input topic
        KStream<String, String> inputTopic = streamBuilder.stream("tweeter_topic");
        KStream<String, String> filteredStream = inputTopic.filter((k, v) -> extractUserFollowersInTweets(v) > 1000);
        filteredStream.to("important_tweets");

        //build the topology
        KafkaStreams kafkaStreams = new KafkaStreams(streamBuilder.build(), properties);

        //start streams

        kafkaStreams.start();
    }

    private static Integer extractUserFollowersInTweets(String tweet) {
        return JsonParser.parseString(tweet)
                .getAsJsonObject()
                .get("user")
                .getAsJsonObject()
                .get("followers_count")
                .getAsInt();

    }
}
